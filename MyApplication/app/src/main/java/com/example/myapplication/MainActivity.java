package com.example.myapplication;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.util.UUID;

public class MainActivity extends AppCompatActivity implements MqttCallback, IMqttActionListener {

    private static final String SERVER_URI = "tcp://broker.hivemq.com:1883";
    private static final String TOPIC = "1n_M3ssag3_xA17E19D0";
    private static final int QOS = 1;
    private static final String TAG = "MainActivity";

    private MqttAndroidClient mqttAndroidClient;

    private TextView textViewSubscribe;
    private final static int REQUEST_CODE_PERMISSION_SEND_SMS = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewSubscribe = (TextView) findViewById(R.id.textInMessage);

        String clientId = UUID.randomUUID().toString();
        //Log.d(TAG, "onCreate: clientId: " + clientId);
        //Toast.makeText(this,"connection clientId: "+clientId,Toast.LENGTH_LONG).show();

        mqttAndroidClient = new MqttAndroidClient(getApplicationContext(), SERVER_URI, clientId);
        mqttAndroidClient.setCallback(this);

        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setCleanSession(false);


        try {
            //Log.d(TAG, "onCreate: Connecting to " + SERVER_URI);
            //Toast.makeText(this,"connection to: "+SERVER_URI,Toast.LENGTH_LONG).show();
            mqttAndroidClient.connect(mqttConnectOptions, null, this);
        } catch (MqttException ex){
            Log.e(TAG, "onCreate: ", ex);
        }

        if (checkPermission(Manifest.permission.SEND_SMS)){
        }else {
            ActivityCompat.requestPermissions(this, new String[]{
                    (Manifest.permission.SEND_SMS)},REQUEST_CODE_PERMISSION_SEND_SMS);
        }

    }

    @Override
    public void onSuccess(IMqttToken asyncActionToken) {
        Toast.makeText(this,"Conexión Exitosa",Toast.LENGTH_LONG).show();
        try {
            mqttAndroidClient.subscribe(TOPIC, QOS);
        } catch (Exception e) {
            Log.e(TAG, "Error subscribing to topic", e);
        }
    }

    @Override
    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
        Toast.makeText(this,"Conexion Fallida",Toast.LENGTH_LONG).show();
    }

    @Override
    public void connectionLost(Throwable cause) {
        Toast.makeText(this,"Conexion perdida",Toast.LENGTH_LONG).show();

    }

    @Override
    public void messageArrived(String topic, MqttMessage message) {
        //Log.d(TAG, "Incoming message: " + new String(message.getPayload()));
        Toast.makeText(this,"Incoming message:"+ new String(message.getPayload()),Toast.LENGTH_LONG).show();
        String split = new String(message.getPayload());
        textViewSubscribe.setText(split);
        String[] parts = split.split(",");
        sendSMS(parts[0],parts[1]);
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
        Toast.makeText(this,"Mensaje completado",Toast.LENGTH_LONG).show();
    }

    public void sendSMS(String numero, String mensaje){
        try {
            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage(numero,null,mensaje,null,null);
            Toast.makeText(getApplicationContext(), "Mensaje Enviado.", Toast.LENGTH_LONG).show();
        }
        catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Mensaje no enviado, datos incorrectos.", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case REQUEST_CODE_PERMISSION_SEND_SMS:
                if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){

                }break;
        }
    }
    private boolean checkPermission(String permission){
        int checkPermission = ContextCompat.checkSelfPermission(this, permission);
        return checkPermission == PackageManager.PERMISSION_GRANTED;
    }

}

